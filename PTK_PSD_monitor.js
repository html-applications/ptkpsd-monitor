/*
��� ��� Monitor
Author: mozers�
------------------------------------------------
�������� ��������� � PTK_PSD\POST\Post\SA:
	�������� card.inf
	�������� � ZIP
	�������� � CAB
	����������� � PTK_PSD\POST

�������� ��������� � PTK_PSD\POST\Post\<311P|364P|365P>:
	������� �������
	���������� ������� (��� ���)
	�������� � ARJ
	�������
	���������� (��� ����������)
	�������� � CAB
	����������� � PTK_PSD\POST

�������� ��������� � PTK_PSD\POST (�������� ������������� ��������):
	�������
	���������� (��� ����������)
	������ � ��
	���������� � POST\Store � POST\Backup
	����������� � BankOfRF\StatInfo\Outcoming

------------------------------------------------

�������� �������� � BankOfRF\StatInfo\Incoming:
	�����������
	������ � ��
	���������� � POST\Store � POST\Backup
------------------------------------------------
*/
var DEBUG = '';
// DEBUG = ' DEBUG';
var version = '2.13.0';
var timeout = 20; //�������� �������� (� ��������)

var stat_key = '2189941022'; // ����������
var stat_abn = '1118'; // ����� �������� (���� �� ������ "111894102201 STAT ����� ��� ��� �� ����� ������ �� ������������� �������" ����� 2189941022\Profile\OPENKEY\941022\lpub.spr

var fns_key  = '0794941009'; // ����� 1 (311�, 322P, 365P)
var fns_abn  = '9020'; // ����� �������� (���� �� ������ "9020941009   ��� 2012 �1" ����� 0794941009\Profile\OPENKEY\941009\lpub.spr

var fts_key  = '0794941009'; // ����� 1 (364�)
var fts_abn  = '9030'; // ����� �������� (���� �� ������ "9030941009   ��� 2012 �1" ����� 0794941009\Profile\OPENKEY\941009\lpub.spr

var ptk_psd_dir = 'P:\\PTK_PSD\\'; // ������� ��������� ��� ���
var SVK_dir = 'C:\\BankOfRF\\StatInfo\\';

// var DSN = 'PTK_PSD';
var post_dir = ptk_psd_dir + 'POST\\';
var database = ptk_psd_dir + 'Database\\etalon97.mdb';  // � �� ������������ �������� (ODBC ����� �� ������������!)

// ��������
var inbox_dir = SVK_dir + 'Incoming\\';

// ���������
var outbox_dir = post_dir + 'Post\\';
var outbox_dir_SA = outbox_dir + 'SA\\';
var outbox_dir_311P = outbox_dir + '311P\\';
var outbox_dir_364P = outbox_dir + '364P\\';
var outbox_dir_322P = outbox_dir + '322P\\';
var outbox_dir_365P = outbox_dir + '365P\\';
var upload_dir = SVK_dir + 'Outcoming\\';

// ������ ����������/������������
var backup_dir = post_dir + 'Backup\\';
var store_dir = post_dir + 'Store\\';

var dt = {};
var post = {};
var logfilename = '';
// =======================================================================
//   C O M M O N
// =======================================================================

// ----------------------------------------
// ����������� ������ <'01',..'zz'> � ����� <1,...1295>
function Chars2Number(chars){
	function Char2Code(i){
		var x = chars.charCodeAt(i);
		if (x > 57) {
			return x-87;
		}else{
			return x-48;
		}
	}
	return Char2Code(0)*36 + Char2Code(1);
}

// ----------------------------------------
// ���������� 1 ������ <'1',..'v'> ��������������� �������� ��� ������
function CharDay(){
	return '0123456789abcdefghijklmnopqrstuv'.charAt(Number(dt.day));
}

// ----------------------------------------
// Return date as object
function objDate(obj){
	function format(x){return (x < 10) ? ('0' + x) : String(x);}
	var datetime = {}; var d;
	if (obj===undefined) {d=new Date();} else {d=new Date(obj);}
	datetime.year = String(d.getYear());                   // yyyy
	datetime.yy = datetime.year.replace(/^../,'');         // yy
	datetime.month = format(d.getMonth()+1);               // mm
	datetime.day = format(d.getDate());                    // dd
	datetime.hours = format(d.getHours());                 // hh
	datetime.min = format(d.getMinutes());                 // mm
	datetime.sec = format(d.getSeconds());                 // ss
	return datetime;
}

// ----------------------------------------
function CurrentDate(){
	return current_date = dt.day+'.'+dt.month+'.'+dt.year;
}

// ----------------------------------------
function TimeString(time){
	function format(x){return (x < 10) ? ('0' + x) : x;}
	var min = format(Math.floor(time/60));
	var sec = format(time%60);
	return min + ':' + sec;
}

// ----------------------------------------
// ���������� ����� ������� � ������� ���
function GetPostNum(post_type){
	// --------------------------------------------------------
	function ReadCounterFile(count_file) {
		if ((FSO.FileExists(count_file)) && (FSO.GetFile(count_file).Size > 0)) {
			with (FSO.OpenTextFile(count_file)) {
				var text = ReadAll();
				Close();
			}
			var arr_lines = text.match(/[^\r\n]+/g);
			for(var i = 0, line; line = arr_lines[i++];) {
				var tmp = line.split('\t');
				Counter[tmp[0]] = {
					number: Number(tmp[1]),
					date: tmp[2]
				};
			}
		}
	}
	// --------------------------------------------------------
	function SaveCounterFile(){
		var lines = [];
		for (var line in Counter){
			lines[lines.length] = line + '\t' + Counter[line].number + '\t' + CurrentDate();
		}
		with (FSO.OpenTextFile(count_file, 2, true)) {
			Write(lines.join('\n'));
			Close();
		}
	}
	// --------------------------------------------------------
	// ����������� ����� <1,...1295> � ������ <'01',..'zz'>
	// ���� one_char=true ��� one_char=1, ��
	// ����������� ����� <1,...31> � ���� ������ <'1',..'v'>
	function Number2Chars(num, one_char){
		function Code2Char(x){
			if (x < 10) {
				return String.fromCharCode(x+48);
			}else{
				return String.fromCharCode(x+87);
			}
		}
		if (one_char) return Code2Char(num%36);
		return Code2Char(num/36) + Code2Char(num%36);
	}
	// --------------------------------------------------------
	var Counter = {};
	var count_file = script_path + 'logs\\counter';
	ReadCounterFile(count_file);
	if (Counter[post_type]) {
		if (Counter[post_type].date == CurrentDate()) {
			Counter[post_type].number++;
		} else {
			Counter[post_type].number = 1;
		}
	} else {
		Counter[post_type] = {};
		Counter[post_type].number = 1;
	}
	SaveCounterFile();
	switch(post_type){
		case 'AFN':
		case 'PSE':
			return ('00' + Counter[post_type].number).replace(/0*(\d{3})$/, '$1');
		default:
			return Number2Chars(Counter[post_type].number);
	}
}

// ----------------------------------------
// �������� ��������� �������������
function Messenger(text){
	function SendMessage(recipient, text){
		WshShell.Run('net send ' + recipient + ' ' + text, 6, false);
	}
	// ��������������
	if (/^[i]/.test(text)){
		text = text.replace(/^i/,'');
		switch(post.type){
			case '1u': // F202
			case '0t': // F212
			case '0d': // F251
			case '1m': // F901
			case '1o': // F904
			case '3h': // F906
			case '3m': // F907
			case '3n': // F908
			case '6z': // F909
			case '1w': // F350
			case '2z': // 311P
			case '5z': // 322P
			case 'mz': // 365P
			case 'kq': // BICSW
				SendMessage('WNNOORG16', text); // ��������
				SendMessage('WNNOORG18', text); // ��������
				SendMessage('WNNOORG17', text); // �������
				break;
			case '1e': // F101
			case '7m': // F128
			case '7e': // F405
			case '0y': // F711
				SendMessage('WNNOORG16', text); // ��������
				SendMessage('WNNOORG18', text); // ��������
				break;
			case '7n': // F129
			case '0c': // F250
			case '7v': // F407
				SendMessage('WNNOORG16', text); // ��������
				SendMessage('WNNOORG18', text); // ��������
				SendMessage('WNNOORG08', text); // ��������������
				break;
			case 'tz': // 364P
			case '0p': // F401
			case '0q': // F402
				SendMessage('WNNOORG16', text); // ��������
				SendMessage('WNNOORG18', text); // ��������
				break;
			case 'sa': // SA �������
				SendMessage('WNNOORG16', text); // ��������
				SendMessage('WNNOORG18', text); // ��������
				break;
			default: // ��� ������
		}
	}
	// �������������� � ������
	SendMessage('NEPTUN', text);    // �������
	SendMessage('WNNOORG05', text); // �������
}

// ----------------------------------------
// ������� ��������� �� �����, ����� ���, �������� ��������� �������������
function Logging(text){
	var dt_log = '['+dt.day+'.'+dt.month+'.'+dt.year+' '+dt.hours+':'+dt.min+':'+dt.sec+'] ';
	if (text=='') dt_log = '';
	WScript.Echo(dt_log + text);
	var file = FSO.OpenTextFile(logfilename, 8, true);
	file.Write(dt_log + text + '\r\n');
	file.Close();
	if (/^[i!]/.test(text)){
		Messenger(text);
	}
}

// ----------------------------------------
function WriteDatabase(filetype, posttype, dt, filename, state_, bik, nkod, error_){
	try {
		var objConnection = new ActiveXObject("ADODB.Connection");
	} catch(e) {
		Logging("!������ ��� �������� ������� ADODB.Connection!");
		return false;
	}
	try {
		var objRecordset = new ActiveXObject("ADODB.Recordset");
	} catch(e) {
		Logging("!������ ��� �������� ������� ADODB.Recordset!");
		return false;
	}
	try {
		objConnection.Open("Driver={Microsoft Access Driver (*.mdb)};DBQ="+database);
		// objConnection.Open("DSN="+DSN);
	} catch(e) {
		Logging("!������ ��� �������� ���������� � ��!");
		return false;
	}

	try {
		with (objRecordset) {
			CursorLocation = 3;
			Open ("SELECT * FROM elo_arh_post" , objConnection, 3, 3);
			AddNew();
			Fields("filetype") = filetype;
			Fields("posttype") = posttype;
			Fields("dt") = dt;
			Fields("filename") = filename;
			Fields("pathname") = "1"; // ��� 2z30b022.745 - ��� ����� ���������� �� ������� ������ �����
			Fields("state_") = state_; //6 - �����������; 7 - �������
			Fields("bik") = bik;
			Fields("nkod") = nkod; //�
			Fields("error_") = error_; // 0 ��� 1 - ���������� ������
			Update();
			Close();
		}
	} catch(err) {
		switch(err.Number){
			case 0:
			case 3709:
				Logging("!������ � ����� ���� ������!");
				return false;
			case -2147217900:
				Logging("!������ � ���������� SQL!");
				return false;
			default:
				Logging("!������ � �������!");
				return false;
		}
	}
	objConnection.Close();
	return true;
}

// ----------------------------------------
function XCopyFile (src, dest, del_src){
	if (!FSO.FolderExists(dest)){
		var dest_dir = '';
		var paths = dest.match(/.*?\\/g);
		for (var i=0; i<paths.length; i++) {
			dest_dir += paths[i];
			if (!FSO.FolderExists(dest_dir)) {FSO.CreateFolder(dest_dir);}
		}
	}
	FSO.CopyFile(src, dest);
	if (del_src) {FSO.DeleteFile(src, true);}
}

// ----------------------------------------
function GetPostInfo(filename){
	function GetIESType(char){
		if (char == '7') { return "���";}
		if (char == '0') { return "���1";}
		if (char == '_') { return "���2";}
		return '';
	}
	function GetFormName(posttype){
		switch(posttype){
			case '1e': return 'F101';
			case '7m': return 'F128';
			case '7n': return 'F129';
			case '1u': return 'F202';
			case '0t': return 'F212';
			case '0c': return 'F250';
			case '0d': return 'F251';
			case '1w': return 'F350';
			case '0p': return 'F401';
			case '0q': return 'F402';
			case '7e': return 'F405';
			case '7v': return 'F407';
			case '0y': return 'F711';
			case '1m': return 'F901';
			case '1o': return 'F904';
			case '3h': return 'F906';
			case '3m': return 'F907';
			case '3n': return 'F908';
			case '6z': return 'F909';
			case '2z': return '311P';
			case 'tz': return '364P';
			case '5z': return '322P';
			case 'mz': return '365P';
			case 'kq': return 'BICSW';
			case 'sa': return 'SA';
			default: return '';
		}
	}
	var filemask = filename.match(/(..)(......)\.([07~])[24][25]/);
	if (!filemask) {return {};}
	post = {};
	post.type = filemask[1];
	// ------------------------------------
	post.name = GetFormName(post.type);
	if (post.name === '') {return {};}
	// ------------------------------------
	var fileinfo = [];
	switch(post.type){
		case '1e': // F101
		case '7m': // F128
		case '7n': // F129
		case '1u': // F202
		case '0t': // F212
		case '0c': // F250
		case '0d': // F251
		case '1w': // F350
		case '0p': // F401
		case '0q': // F402
		case '7e': // F405
		case '7v': // F407
		case '0y': // F711
		case '1m': // F901
		case '1o': // F904
		case '3h': // F906
		case '3m': // F907
		case '3n': // F908
		case '6z': // F909
		case '5z': // 322P
		case 'mz': // 365P
		case 'kq': // BICSW
		case 'sa': // SA �������
		case '2z': // 311P
		case 'tz': // 364P
			fileinfo = filemask[2].match(/([1-9a-v])([0-9a-z]{2})([0_7])[24][25]/i);
			post.number = Chars2Number(fileinfo[2]);
			break;
	}
	if (post.number === '') {return {};}
	// ------------------------------------
	// post.ies_type = GetIESType(fileinfo[3]);
	post.ies_type = (filemask[3]=='~') ? '���3' : GetIESType(fileinfo[3]);
	if (post.ies_type === '') {return {};}
	// ------------------------------------
	return post;
}

// ----------------------------------------
// ������� ������ ������ � �������� �� ��������� �����
function GetFileList(dir, re_file_mask, remove_no_mask){
	var arr_files = [];
	var folder = FSO.GetFolder(dir);
	var files = new Enumerator(folder.Files);
	for (;!files.atEnd();files.moveNext()){
		var file = files.item();
		var filepath = file.Path;
		var filename = FSO.GetFileName(filepath);
		if (re_file_mask.test(filename)) {
			arr_files[arr_files.length] = filename;
		} else {
			if (remove_no_mask) {
				Logging('!������� ��������� ����: ' + filepath);
				FSO.DeleteFile(filepath, true);
			}
		}
	}
	return arr_files;
}

// ----------------------------------------
function SetSignature(path, filename, key){
	Logging(' ������� '+filename+' ������ '+key+'01');
	var ret = WshShell.Run('"' + script_path + '_SignMake.cmd" "' + path + filename + '" ' + key + DEBUG, 6, true);
	if (ret !== 0) {
		Logging('!������ ��� ��������� ������� '+filename);
		return false;
	}
	return true;
}

// ----------------------------------------
function Encrypt(path, filename, key, abonent){
	function AbonentName(abn){
		switch(abn){
			case stat_abn: return ' (STAT)';
			case fns_abn:  return ' (���)';
			case fts_abn:  return ' (���)';
		}
		return '';
	}
	// -----------------------------------------------
	Logging(' ���������� '+filename+' ������ '+key+' �� �������� '+ abonent + AbonentName(abonent));
	var ret = WshShell.Run('"' + script_path + '_Encrypt.cmd" "' + path + filename + '" ' + key + ' ' + abonent + DEBUG, 6, true);
	if (ret !== 0) {
		Logging('!������ ��� ���������� '+filename);
		return false;
	}
	return true;
}

// ----------------------------------------
// ����������� ����� ��������, ��������������� ��������� �����, � 1 ���� ARJ
// ���� del_src = true, �� �������� ����� ���������
function ArjPack(path, re_file_mask, filename_arj, del_src){
	var files_pack = GetFileList(path, re_file_mask);
	if (files_pack.length==0) return false;
	with (FSO.OpenTextFile(path + "files.lst", 2, true)) {
		Write(files_pack.join('\r\n'));
		Close();
	}
	// Make ARJ
	Logging(' �������� � ' + files_pack.length + ' ������ � ARJ ' + filename_arj);
	var ret = WshShell.Run('cmd /c PUSHD "' + path + '" && "' + script_path + '\\bin\\arj32.exe" a -e ' + filename_arj + ' !files.lst && DEL /Q /F files.lst & POPD', 6, true);
	if (ret !== 0) {
		Logging('!������ ��� �������� � ARJ ' + filename);
		return false;
	}
	if (del_src) {
		for (var i=0; i<files_pack.length; i++) FSO.DeleteFile(path + files_pack[i], true);
	}
	return true;
}

// ----------------------------------------
// ����������� ����� ��������, ��������������� ��������� �����, � 1 ���� CAB
// ���� del_src = true, �� �������� ����� ���������
function CabPack(path, re_file_mask, filename_cab, del_src){
	var files_pack = GetFileList(path, re_file_mask);
	if (files_pack.length==0) return false;
	with (FSO.OpenTextFile(path + "setup.ddf", 2, true)) {
		Write('.set CabinetNameTemplate="' + filename_cab + '"\r\n.Set DiskDirectoryTemplate="."\r\n' + files_pack.join('\r\n'));
		Close();
	}
	// Make CAB
	Logging(' �������� � ' + files_pack.length + ' ������ � CAB ' + filename_cab);
	var ret = WshShell.Run('cmd /c PUSHD "' + path + '" && MAKECAB /f setup.ddf && DEL /Q /F setup.* & POPD', 6, true);
	if (ret !== 0) {
		Logging('!������ ��� �������� � CAB ' + filename);
		return false;
	}
	if (del_src) {
		for (var i=0; i<files_pack.length; i++) FSO.DeleteFile(path + files_pack[i], true);
	}
	return true;
}

// ----------------------------------------
// ���������� ����� � �������� Store (��� del_src=true �������� ���������)
// ������ � ��� � ��
function KeepToStore(path, filename, del_src, error_) {
	var filename_store = filename + '.' + dt.hours + dt.min + dt.sec;
	var filepath_store = store_dir + dt.year + '\\' + dt.month + '\\' + dt.day + '\\' + filename_store;
	var info = del_src ? ' ����������� ' : ' ����������� ';
	Logging(info + filename + ' � Store');
	XCopyFile(path + filename, filepath_store, del_src);

	// Write to Database
	var post = GetPostInfo(filename);
	var datetime = dt.year+'/'+dt.month+'/'+dt.day+' '+dt.hours+':'+dt.min+':'+dt.sec;
	var ret;
	if ((post.ies_type == '���') || (post.ies_type == '���3')) {
		Logging(' ������ � ��');
		ret = WriteDatabase(post.ies_type, post.type, datetime, filename_store, "6", "42202745", post.number, "0");
	} else {
		Logging(' ������ � �� � �������� "' + ((error_ == 0) ? '' : '�� ') + '������"');
		ret = WriteDatabase(post.ies_type, post.type, datetime, filename_store, "7", "42200022", post.number, error_);
	}
	if (!ret) Logging('!������ �������� ������ � ��!');
}

// =======================================================================
//   ����� (�������� ����������� �� �� ���������)
// =======================================================================
// ����������� ���������� ������� (��������� ��� ��� ��� - �� �����)
// ���������� 1 ���� ����� ������ �������� ��������� � ������. ����� - 0;
function CheckErrorContent(filepath){
	Logging(' ������ ������ ������');
	var text = WshShell.Exec('"' + script_path + '_GetTextContent.cmd" "' + filepath + '"' + DEBUG).StdOut.ReadAll();
	if (/###SB\w+\.txt###/i.test(text)) {              // 311�
		if (/^����訡��:(\d{3})/m.test(text)) {  // '���������:' (cp866)
			if (RegExp.lastParen != '000') return 1;
		}
	}
	else
	if (/###(IZV|KWT)\w+\.(txt|vrb)###/i.test(text)) {  // 365�
		if (/^\d\d;[^@]+@@@$/m.test(text)) return 1;
	}
	else
	{
		if (/�� ������|�� �ਭ��/.test(text)) return 1; // ��� ���������
	}
	return 0;
}

// ----------------------------------------
function CheckInBox() {
	var files = GetFileList(inbox_dir, /.+/);
	if (files.length == 0) return false;
	for (var i=0; i<files.length; i++) {
		var filename = files[i];
		var filepath = inbox_dir + filename;
		var post = GetPostInfo(filename);
		if (post.type === undefined) {
			Logging('!�� ������� �������� - ����������� ���� '+filename+ '. ���������� � \\undefined');
			var undef_dir = inbox_dir + 'undefined\\';
			if (!FSO.FileExists(undef_dir)) WshShell.Run('cmd /c md "' + undef_dir + '"',0 , true);
			FSO.MoveFile(filepath, undef_dir);
			return;
		}
		// -----------------------------------------------------------------
		Logging('i������� ' + post.ies_type + ' �� ����� ' + post.name + ' (������� � ' + post.number + ') ' + filename);
		// -----------------------------------------------------------------
		// Copy to Backup
		Logging(' ����������� '+filename+ ' � Backup');
		XCopyFile(filepath, backup_dir + dt.year + dt.month + dt.day + '\\', false);
		// Decrypt
		Logging(' ����������� '+filename);
		var ret = WshShell.Run('"' + script_path + '_Decrypt.cmd" "' + filepath + '"' + DEBUG, 6, true);
		if (ret !== 0) {
			Logging('!������ ����������� ����� '+filename);
			return;
		}

		var error_ = 0;  //1 - ���������� ������

		if (post.type == "mz") {
			Logging(' ����������� ������� 365� '+filename);
			var ret_365PRePack = WshShell.Run('"' + script_path + '_365PRePack.cmd" "' + filepath + '"' + DEBUG, 6, true);
			switch(ret_365PRePack){
				case 365:
					Logging(' ������� �������� VRB ���������');
					error_ = 1; // �������� ������� 365� ����� ����� ��� "�� �������"
					// ������������ ������������� ���3:
					try {
						var arj_name = WshShell.RegRead("HKCU\\Environment\\365");
						var text = '������� ' + dt.day + '.' + dt.month + '.' +  dt.year + ' ' + dt.hours + ':' + dt.min + ':' + dt.sec + ' � �������:\n' + arj_name;
						var file_IES365 = outbox_dir + filename.replace(/^(\w{5})\w{3}\.(\w{3})$/, '$1$2.~22');
						with (FSO.OpenTextFile(file_IES365, 8, true)) {
							Write(text);
							Close();
						}
					} catch(e) {
						Logging('!������ ��� �������� ���3 �� ������� 365� ' + filename);
					}
					break;
				case 0:
					break;
				default:
					Logging('!������ ����������� ������� 365� ' + filename);
					return;
			}
		}

		if (((post.ies_type == "���1") || (post.ies_type == "���2")) && (error_ == 0)) {
			error_ = CheckErrorContent(filepath); //1 - ���������� ������
		}

		KeepToStore(inbox_dir, filename, true, error_);
		Logging('');
	}
}

// =======================================================================
//   �������� (�������� �������������� � ��� ��� ���������)
// =======================================================================
// �������� ������������� �������� ��� ���������
// encrypt_files - ������ ���� ������, ������� ������� ����������
function MakeNalogPack(dir, encrypt_files, key, abn, filename_arj, filename_cab){
	for (var i=0; i<encrypt_files.length; i++) {
		if (!Encrypt(dir, encrypt_files[i], key, abn)) return false; // ������� ��
	}
	var del_src = (encrypt_files.length > 0);
	if (!ArjPack(dir, /.+\.(txt|xml|vrb)$/i, filename_arj, del_src)) return false; // ����������� ��� � arj
	if (!SetSignature(dir, filename_arj, stat_key))        return false; // ������� arj
	if (!CabPack(dir, /.+\.arj$/i, filename_cab, true))    return false; // � ����������� ������� CAB
	return true;
}

// ----------------------------------------
function CheckNalogOutbox(current_outbox, subdir){
	var re_files_txt_mask;     //  RegExp-����� ��� ������ �������������� ������
	var re_encrypt_files_mask; // RegExp-����� ��� ������ ������ ��������� ����������
	var filename_arj;          // ��� ARJ �����
	var filename_cab;          // ��� ������������� �������� CAB
	var key;
	var abn;
	var ptype;
	switch(current_outbox){
		case outbox_dir_311P: // 311� =============================
			current_outbox = current_outbox + subdir + '\\';
			re_files_txt_mask = /^SBC\d\d2202745_5260\d{8}_1637\d{4}\d{2}\d{6}_\d{3}\.txt$/i;
			ptype = 311;
			break;
		case outbox_dir_364P: // 364� =============================
			re_files_txt_mask = /^PS\d{8}_\d{4}_\d{4}_\d_\d_\d{4}_\d{4}.xml$/i;
			ptype = 364;
			break;
		case outbox_dir_365P: // 356� =============================
			re_files_txt_mask = /^(IZV|PB1|PB2|BOS|BUV|BV)[\w_]+\.(txt|vrb)$/i;
			ptype = 365;
			break;
	}

	files_txt = GetFileList(current_outbox, re_files_txt_mask, true);
	if (files_txt.length == 0) return false;

	Logging(' �������� ���������� �������� (��� ����������):');
	for (var i=0; i<files_txt.length; i++) {
		if (!SetSignature(current_outbox, files_txt[i], stat_key)) return false; // ������� �� ������
	}

	var encrypt_files = []; // ������ ������, ��������� ����������
	switch(ptype){
		case 311: // 311� =============================
			re_encrypt_files_mask = re_files_txt_mask;
			key = fns_key;
			abn = fns_abn;
			filename_arj = subdir + '02745' + dt.yy + dt.month + dt.day + GetPostNum('ABCD') + '.arj';
			filename_cab = '2z' + CharDay() + GetPostNum('2z') + '745.022';
			break;
		case 364: // 364� =============================
			re_encrypt_files_mask = re_files_txt_mask;
			key = fts_key;
			abn = fts_abn;
			filename_arj = 'PSEI_1637_0007_' + dt.year + dt.month + dt.day + '_' + GetPostNum('PSE') + '.arj'; //001 - ���������� ����� (001-999) ��������� �����
			filename_cab = 'tz' + CharDay() + GetPostNum('tz') + '745.022';
			break;
		case 365: // 356� =============================
			re_encrypt_files_mask = /^(BOS|BV).+\.vrb$/i;
			key = fns_key;
			abn = fns_abn;
			filename_arj = 'AFN_2202745_MIFNS00_' + dt.year + dt.month + dt.day + '_' + GetPostNum('AFN') + '.arj'; //001 - ���������� ����� (001-999) ��������� �����
			filename_cab = 'mz' + CharDay() + GetPostNum('mz') + '745.022';
			break;
	}

	if (!MakeNalogPack(current_outbox, encrypt_files, key, abn, filename_arj, filename_cab)) return false; // ������ ������������ cab ��� ����������
	// (��� Store - ����� � �����, � ��� ��������, ���� �����������, ������� ��-�����)

	var encrypt_files = GetFileList(current_outbox, re_encrypt_files_mask);
	if (encrypt_files.length == 0) { // ���� ������, ��������� ����������, ���
		KeepToStore(current_outbox, filename_cab);
		for (var i=0; i<files_txt.length; i++) FSO.DeleteFile(current_outbox + files_txt[i], true);
	} else {
		KeepToStore(current_outbox, filename_cab, true);
		// � �������� �������� ��� ����������� txt �����
		Logging(' �������� ���������� �������� (� �����������):');
		if (!MakeNalogPack(current_outbox, encrypt_files, key, abn, filename_arj, filename_cab)) return false;
	}
	Logging(' ����������� ' + filename_cab + ' � Post');
	FSO.MoveFile(current_outbox + filename_cab, outbox_dir);
	Logging('');
}

// ----------------------------------------
function CheckSAOutBox() {
	// ���������� �������� ����� ����������
	function GetFormName(filename){
		if (/^Z745212\.doc$/i.test(filename)) return '212';
		if (/^ZDL16370007\.xls$/i.test(filename)) return 'ZDL';
		if (/^mn201745\.xls$/i.test(filename)) return 'mn';

		if (/^16370007\.n[0-1]\d$/i.test(filename)) return '308P';
		if (/^tl[0-1]\d[0-1]\d1637F7\.txt$/i.test(filename)) return '601';
		if (/^1637_7\.T[0-3]\d$/i.test(filename)) return '652';
		if (/^16370007\.O[0-1]\d$/i.test(filename)) return '664';
		if (/^16370007\.xml$/i.test(filename)) return '665';
		if (/^PS16370007_[0-1]\d[0-1]\d[0-3]\d\.arj$/i.test(filename)) return 'PS';
		if (/^VBK16370007_[0-1]\d[0-1]\d[0-3]\d\.arj$/i.test(filename)) return 'VBK';
		if (/^KR_16370007_[0-1]\d[0-1]\d[0-3]\d\.arj$/i.test(filename)) return 'KR';
	}
	// ���������� ��� zip �����
	function GetZipName(form_name){
		switch(form_name){
			case '308P': return 'F745308P.zip';
			case '601' : return 'F745601.zip';
			case '652' : return 'F745652.zip';
			case '664' : return 'F745664.zip';
			case '665' : return 'F745665.zip';
			case 'PS'  : return 'F745PS.zip';
			case 'VBK' : return 'F745VBK.zip';
			case 'KR'  : return 'F745KR.zip';
		}
	}
	// �������� zip �����
	function CreateZipFile(source, zip_file){
		if (!FSO.FileExists(zip_file)){
			with (FSO.CreateTextFile(zip_file, true)){
				Write(String.fromCharCode(80,75,5,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0));
				Close();
			}
		}
		zip_file = FSO.GetAbsolutePathName(zip_file);
		source = FSO.GetAbsolutePathName(source);
		var file_size = FSO.GetFile(zip_file).Size;
		Shell.NameSpace(zip_file).CopyHere(source);
		do {
			WScript.Sleep(100);
			try {
				if (FSO.GetFile(zip_file).Size!=file_size) return;
			} catch(e) {}
		} while (true);
	}
	// ----------------------------------------------------------
	var files = GetFileList(outbox_dir_SA, /^.+$/i);

	var cardinf_list = '';
	for (var i=0; i<files.length; i++) {
		var filename = files[i];
		var form_name = GetFormName(filename);
		if (form_name !== undefined) {
			switch(form_name){
				case '212':
					Logging(' ��������� ���� ' + filename + ' (������������� ������� � ����� 212)');
					cardinf_list += filename + ', ����: \r\n';
					break;
				case 'ZDL':
					Logging(' ��������� ���� ' + filename + ' (���������� � ������������ �������������)');
					cardinf_list += filename + ', ����: \r\n';
					break;
				case 'mn':
					Logging(' ��������� ���� ' + filename + ' (����� �� ��������� ������)');
					cardinf_list += filename + ', ����: \r\n';
					break;
				default:
					// Zip-��� ��� ������ �����
					Logging(' ��������� ���� ' + filename + ' (����� �� ����� ' + form_name + ')');
					var zip_file = GetZipName(form_name);
					CreateZipFile(outbox_dir_SA + filename, outbox_dir_SA + zip_file);
					FSO.DeleteFile(outbox_dir_SA + filename, true);
					Logging(' ���� �������� � ����� ' + zip_file);
					cardinf_list += zip_file + ', ����: \r\n';
			}
		} else {
			Logging('!��������������������� ���� ' + outbox_dir_SA + filename);
		}
	}
	if (cardinf_list != '') {
		Logging(' �������� SA �������');
		// ������� card.inf
		with (FSO.CreateTextFile(outbox_dir_SA +'\\card.inf', true)){
			Write('42202745\r\n42200022\r\n'+dt.day+'.'+dt.month+'.'+dt.yy+'\r\n'+files.length+'\r\n'+cardinf_list);
			Close();
		}
		var day = '0123456789abcdefghijklmnopqrstuv'.charAt(Number(dt.day));
		var filename_cab = 'sa' + day + GetPostNum('SA') + '745.022';
		CabPack(outbox_dir_SA, /^.+$/i, filename_cab, true);
		Logging(' ����������� ' + filename_cab + ' � Post');
		FSO.MoveFile(outbox_dir_SA + filename_cab, outbox_dir);
		Logging('');
	}
}

// ----------------------------------------
function CheckOutBox() {
	var files = GetFileList(outbox_dir, /^.+$/i);
	for (var i=0; i<files.length; i++) {
		var filename = files[i];
		post = GetPostInfo(filename);
		if (post.type !== undefined) {
			Logging(' �������� ������������� ��������:');
			// Copy to Store
			switch(post.type){
				case '2z': // 311P
				case 'mz': // 365P
				case 'tz': // 365P
					if (post.ies_type != '���3') break; // ��� ����������� ����� (� ��������������� ����)
				default:
					KeepToStore(outbox_dir, filename, false);
			}
			// Make Signature
			if (!SetSignature(outbox_dir, filename, stat_key)) return;
			// Encrypt
			if (!Encrypt(outbox_dir, filename, stat_key, stat_abn)) return;
			// Copy to Outbox
			Logging(' ����������� '+filename+ ' � Outbox');
			FSO.CopyFile(outbox_dir + filename, upload_dir);
			// Move to Backup
			Logging(' ����������� '+filename+ ' � Backup');
			XCopyFile(outbox_dir + filename, backup_dir + dt.year + dt.month + dt.day + '\\', true);
			Logging('i������������ ' + post.ies_type + ' - ����� ' + post.name + ' (������� � ' + post.number + ') ' + filename);
			Logging('');
		} else {
			Logging('!��������������������� ���� � Outbox '+filename);
		}
	}
}

// ============================================================================

var WshShell = new ActiveXObject("WScript.Shell");
var FSO = new ActiveXObject("Scripting.FileSystemObject");
var Shell = new ActiveXObject("Shell.Application");

var script_path = FSO.GetParentFolderName(WScript.ScriptFullName) + '\\';
var out = script_path + '\\temp\\out.txt';

// INIT
if (WshShell.Run(script_path + '_StartVFD.cmd' + DEBUG, 6, true) !== 0) {WScript.Quit(1);}

if (WScript.Arguments.length === 0){
	var title = '��� ��� Monitor v.' + version;
	WshShell.Run('cmd /f:on /t:1b /k title=' + title + ' & cscript /nologo "' + WScript.ScriptFullName + '" � mozers�');
	WScript.Quit();
}

// ----------------------------------------
// �������� ���������
function Process(){
	dt = objDate();
	logfilename = script_path + "logs\\" + dt.year + dt.month + dt.day + ".log";
	if (FSO.FolderExists(outbox_dir)) {
		CheckSAOutBox();
		CheckNalogOutbox(outbox_dir_311P, 'A');
		CheckNalogOutbox(outbox_dir_311P, 'B');
		CheckNalogOutbox(outbox_dir_311P, 'C');
		CheckNalogOutbox(outbox_dir_311P, 'D');
		CheckNalogOutbox(outbox_dir_364P);
		CheckNalogOutbox(outbox_dir_365P);
		CheckOutBox();
	}else{
		Logging('!���������� ������� '+outbox_dir);
	}
	if (FSO.FolderExists(inbox_dir)) {
		CheckInBox();
	}else{
		Logging('!���������� ������� '+inbox_dir);
	}
}

var sec;
do { // ����������� ������
// ---------------------------------------------------------------------------------
	WScript.StdOut.Write('                                                    \r');
	Process();
	sec = timeout;
	do {
		WScript.StdOut.Write('�� ��������� �������� �������� ' + TimeString(sec) + '\r');
		WScript.Sleep(1000);
		sec--;
	} while (sec > 0);
// ---------------------------------------------------------------------------------
} while (true);
