/*
Get Crypto
Version: 2.3.0
Author: mozers�
------------------------------------------------
Description:
���������� ����� ���������� � ������� �����
�������������: _GetCrypto.js filename
------------------------------------------------
*/

function GetCryptoInfo(filename) {
	var file = {};
	var Stream = new ActiveXObject("ADODB.Stream");
	Stream.type = 2;
	Stream.charset = 'ibm866';
	Stream.open();
	Stream.loadFromFile(filename);
	Stream.Position = 0;
	var data = Stream.readText(7);
	if (/a(\d{6})/.test(data)){
		file.key = RegExp.lastParen;
		var abonents = [];
		var pos = 47;
		var i = -1;
		do {
			i++;
			Stream.Position = pos;
			abonents[i] = Stream.readText(4);
			pos = pos + 48;
		} while (/\d{4}/.test(abonents[i]));
		abonents.pop();
		file.from = abonents.pop();
		file.to = abonents.join(" ");
	}
	// Get Signature
	var content = Stream.readText();
	Stream.close();
	var arr = content.match(/o000000(?:.|\n){64}(\d{4}[0-9A-Z]{6}\d{2})(?:.|\n){15}$/);
	if (arr) file.sign = arr[1];
	return file;
}

var arg = WScript.Arguments;
if (arg.length==0) {
	WScript.Echo('Require argument - filename!');
	WScript.Quit(1);
}

var filepath = arg(0);
var FSO = new ActiveXObject("Scripting.FileSystemObject");
if (!FSO.FileExists(filepath)) {
	WScript.Echo('File '+filepath+ ' not found!');
	WScript.Quit(1);
}

var crypto = GetCryptoInfo(arg(0));
var output = '';
for (var i in crypto){
	output += i + '\t' + crypto[i] + '\r\n';
}

WScript.Echo(output);
