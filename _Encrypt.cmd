@echo off
:: Encrypt
:: Version: 1.2.0
:: Author: mozers
:: ------------------------------------------------
:: Description:
:: ����஢뢠�� 䠩� (����室�� ����㠫�� ��᪮���)
:: �ᯮ�짮�����: _Encrypt.cmd sau01745.022 2187941022 1114
:: ------------------------------------------------

set VDrive=B:
set scr_path=%~dp0
SET wftesto=%~dp0bin\wftesto.exe
TITLE %~n0

:: �஢�ઠ �室��� ��ࠬ��஢ --------------------------------------------
if not exist %VDrive%\ (
	echo ����㠫�� ��� %VDrive% �� ������!
	GOTO :Error
)

if "%1"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ��� 䠩��!
	GOTO :Error
)

if "%2"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ���� ��஢����!
	GOTO :Error
) else (
	set key=%2
)

if "%3"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ����� �����!
	GOTO :Error
) else (
	set to=%3
)

if not exist %1 (
	echo ���� %1 �� ������!
	GOTO :Error
)

:: �஢�ઠ ��⥩ � ���砬 -----------------------------------
set key_path=%scr_path%Verba\%key%\Profile
if not exist %key_path% (
	echo ���� %key_path% �� ������!
	GOTO :Error
)
set skey_path=%scr_path%Verba\%key%\floppy
if not exist %skey_path% (
	echo ���� %skey_path% �� ������!
	GOTO :Error
)

:: ����㧪� ���� ��஢���� -------------------------------------
%wftesto% l > nul
if errorlevel 1 asrkeyw

call :GetSlotKey

if not "%exist_key%"=="%key%" (
	if not "%exist_key%"=="" (
		echo ����塞 ���� ���� �� 0 ᫮�
		%wftesto% r %exist_key%
		if errorlevel 1 GOTO :Error
	)
	echo ����㦠�� � 0 ᫮� ���� %key%
	RMDIR /s /q %VDrive%\
	XCOPY %skey_path% %VDrive%\ /S /Y /Q > nul 2<&1
	%wftesto% i %key% %VDrive%
	if errorlevel 1 GOTO :Error
	RMDIR /s /q %VDrive%\
)

:: ���஢���� ------------------------------------------------------
echo ����஢뢠�� ���箬 %key% �� ������� %to%
%wftesto% e %1 %1 %key_path%\ %key_path% %key:~,4% %to%
if errorlevel 1 GOTO :Error

if not [%4]==[] (
	echo.
	echo  [Press any key to exit]
	pause > nul
)
EXIT /b 0

:: ����� ���� ����㦥� � 0 ᫮� ? --------------------------------
:GetSlotKey
FOR /F "tokens=1,2 delims=: " %%a IN ('%wftesto% l') DO (
	if "%%a"=="Slot" (if not "%%b"=="0" goto :eof)
	if "%%a"=="NUM" (
		if not "%%b"=="" (
			echo � 0 ᫮� - ���� ��஢���� %%b
			SET exist_key=%%b
			goto :eof
		)
	)
	if "%%a"=="NUMP" (
		if not "%%b"=="" (
			echo � 0 ᫮� - ���� ������ %%b
			SET exist_key=%%b
			goto :eof
		)
	)
)
echo � 0 ᫮� - ��� ���祩
goto :eof

:: ------------------------------------------------------
:Error
echo.
echo  [Press any key to exit]
pause > nul
EXIT 1

