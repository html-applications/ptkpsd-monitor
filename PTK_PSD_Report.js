/*
���������� ������ ���������� ��� ��������
Version: 2.1.1
Author: mozers�
------------------------------------------------
*/

var WshShell = new ActiveXObject("WScript.Shell");
var FSO = new ActiveXObject("Scripting.FileSystemObject");
var Shell = new ActiveXObject("Shell.Application");

var title = "�������� ���������� � ��� ���";
var ptkpsd_outbox = "P:\\PTK_PSD\\POST\\Post\\";  // ������� ���� �������� ������� �������

// =================================================================
var vb = {};

vb.Function = function(func){
	return function()    {
		return vb.Function.eval.call(this, func, arguments);
	};
};

vb.Function.eval = function(func){
	var args = Array.prototype.slice.call(arguments[1]);
	for (var i = 0; i < args.length; i++) {
		if ( typeof args[i] != 'string' ) {
			continue;
		}
		args[i] = '"' + args[i].replace(/"/g, '" + Chr(34) + "').replace(/\r\n/g, '" + vbCrLf + "').replace(/\r/g, '" + vbCr + "').replace(/\n/g, '" + vbLf + "') + '"';
	}

	var vbe;
	vbe = new ActiveXObject('ScriptControl');
	vbe.Language = 'VBScript';

	return vbe.eval(func + '(' + args.join(', ') + ')');
};

// ����������� VB ������� InputBox(prompt[, title][, default][, xpos][, ypos][, helpfile, context])
var InputBox = vb.Function('InputBox');
// ---------------------------------------------------------------------------------
// �������� ������ � SendTo
function CreateShortcut(){
	with (WshShell.CreateShortcut(WshShell.SpecialFolders("SendTo") + "\\��� ���.lnk")){
		TargetPath = WScript.ScriptFullName;
		IconLocation = 'p:\\PTK_PSD\\Script\\PTK_PSD_Browser\\icon.ico';
		Description = "�������� ������������ ����� ��� ���";
		Save();
	}
	WshShell.Popup('����� ����������\n���������� � ����������� ���� "���������"!', 3, '��� ���: �������� ������', 64);
}

// ���������� �������� ����� ����������
function GetFormName(filename){
	if (/^Z745212\.doc$/i.test(filename)) return '212';
	if (/^ZDL16370007\.xls$/i.test(filename)) return 'ZDL';
	if (/^mn201745\.xls$/i.test(filename)) return 'mn';
	if (/^16370007\.n[0-1]\d$/i.test(filename)) return '308P';
	if (/^tl[0-1]\d[0-1]\d1637F7\.txt$/i.test(filename)) return '601';
	if (/^1637_7\.T[0-3]\d$/i.test(filename)) return '652';
	if (/^16370007\.O[0-1]\d$/i.test(filename)) return '664';
	if (/^16370007\.xml$/i.test(filename)) return '665';
	if (/^PS16370007_[0-1]\d[0-1]\d[0-3]\d\.arj$/i.test(filename)) return 'PS';
	if (/^VBK16370007_[0-1]\d[0-1]\d[0-3]\d\.arj$/i.test(filename)) return 'VBK';
	if (/^KR_16370007_[0-1]\d[0-1]\d[0-3]\d\.arj$/i.test(filename)) return 'KR';
	if (/^SBC\d\d2202745_5260\d{8}_1637\d{4}\d{2}\d{6}_\d{3}\.txt$/.test(filename)) return '311P';
	if (/^(BOS\d|BV\d{3})_(ZNO|RPO|RBN)12202745_\d{12}_\d{6}\.vrb$/.test(filename)) return '365P';
	if (/^PB2_(ZNO|ROO|RPO|RBN)12202745_\d{12}_\d{6}\.txt$/.test(filename)) return '������������� 365P';
	if (/^PS\d{8}_\d{4}_\d{4}_\d_\d_\d{4}_\d{4}\.xml$/.test(filename)) return '364P';
}

// ����������� ����� � ptkpsd_outbox
function CopyToOutbox(file, dest){
	if (FSO.GetFolder(dest).Files.Count == 0) { // ���� � �������� ���������� �����
		FSO.CopyFile(file, dest + '\\');
		var filename = FSO.GetFileName(file);
		WshShell.Popup('���� ���������� �� ����� ' + GetFormName(filename) + ':\n' + filename + '\n\n ������� ��������� � ��� ���!', 10, title, 64);
		WScript.Quit();
	} else { // ���� � �������� ���������� ������� ��� �������������� �����, �� - ����...
		WScript.Sleep(2000);
		CopyToOutbox(file, dest);
	}
}

// ����������� ���� ������ 665 ����� �� �������� �������� � ptkpsd_outbox
function CopyAllFiles(filepath){
	var dest = ptkpsd_outbox + 'SA';
	if (FSO.GetFolder(dest).Files.Count == 0) { // ���� � �������� ���������� �����
		var path = FSO.GetParentFolderName(filepath);
		var folder = FSO.GetFolder(path);
		var files = new Enumerator(folder.Files);
		var filelist = [];
		for (; !files.atEnd(); files.moveNext()){
			var file = files.item();
			switch(GetFormName(file.Name)){
				case '665':
				case 'KR':
				case 'PS':
				case 'VBK':
					FSO.CopyFile(file.Path, dest + '\\');
					filelist[filelist.length] = file.Name;
					break;
			}
		}
		WshShell.Popup('����� ���������� �� ����� 665:\n' + filelist.join('\n') + '\n\n ������� ���������� � ��� ���!', 10, title, 64);
		WScript.Quit();
	} else { // ���� � �������� ���������� ������� ��� �������������� �����, �� - ����...
		WScript.Sleep(2000);
		CopyAllFiles(filepath);
	}
}

// =================================================================================
var count = WScript.Arguments.length;
if (count==0) {
	CreateShortcut();
	WScript.Quit();
} else {
	var files = [];
	for (var i=0; i<count; i++) {
		files[i] = WScript.Arguments(i);
	}
}

for (var i=0; i<count; i++) {
	var filepath = files[i];
	switch(GetFormName(FSO.GetFileName(filepath))){
		case '665':
		case 'PS':
		case 'VBK':
		case 'KR':
			if ((count==1) && (WshShell.Popup("�������� � ��� �� ������� �����:\n- PS (�������� ������),\n- KR (��������� ��������),\n- VBK (��������� ����������� ��������),\n �� �������� ��������?", 0, "�������� 665 �����", 36)==6)) CopyAllFiles(filepath);
		case '212':
		case 'ZDL':
		case 'mn':
		case '308P':
		case '601':
		case '652':
		case '664':
			CopyToOutbox(filepath, ptkpsd_outbox + 'SA');
			break;
		case '364P':
			CopyToOutbox(filepath, ptkpsd_outbox + '364P');
			break;
		case '365P':
		case '������������� 365P':
			CopyToOutbox(filepath, ptkpsd_outbox + '365P');
			break;
		case '311P':
			var type = InputBox("������� ��� ������������� �����:\n1\t��� �������� � ���, �� � ���\n2\t��� �������� � ���\n3\t��� �������� � ��\n4\t��� �������� � ���", "����� 311�", "1");
			switch(type){
				case '1': 
					CopyToOutbox(filepath, ptkpsd_outbox + '311P\\A');
					break;
				case '2':
					CopyToOutbox(filepath, ptkpsd_outbox + '311P\\B');
					break;
				case '3':
					CopyToOutbox(filepath, ptkpsd_outbox + '311P\\C');
					break;
				case '4':
					CopyToOutbox(filepath, ptkpsd_outbox + '311P\\D');
					break;
				default:
					WshShell.Popup("��� ������������� ����� �� ������\n��� ������ �������!", 10, "������ ��� ��������", 16);
			}
			break;
		default:
			WshShell.Popup("����\n" + FSO.GetFileName(filepath) + "\n�� �������� ������ ����������!", 0, "������ ��� ��������", 16);
	}
}
