@ECHO OFF
:: Start VFD
:: Version: 1.2.0
:: Author: mozers
:: ------------------------------------------------
:: Description:
:: ����� �ࠩ��� � ����஢���� ��᪠ B: ����㠫쭮�� ��᪮����
:: ���樠������ ���稪� ��砩��� �ᥫ
:: ------------------------------------------------

TITLE %~n0
set vfd=%~dp0bin\vfd.exe
SET wftesto=%~dp0bin\wftesto.exe
::----------------------------------
if not exist B: (
	%vfd% install
	%vfd% start
	%vfd% link B /L
	%vfd% open B: /144
)

%wftesto% l > nul
if errorlevel 1 asrkeyw

if not [%1]==[] (
	echo.
	echo  [Press any key to exit]
	pause > nul
)
