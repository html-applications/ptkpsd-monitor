@echo off
:: Make Signature
:: Version: 1.1.0
:: Author: mozers
:: ------------------------------------------------
:: Description:
:: ������뢠�� 䠩� (����室�� ����㠫�� ��᪮���)
:: �ᯮ�짮�����: _SignMake.cmd sau01745.022 2187941022
:: ------------------------------------------------

set VDrive=B:
set scr_path=%~dp0
SET wftesto=%~dp0bin\wftesto.exe
TITLE %~n0

:: �஢�ઠ �室��� ��ࠬ��஢ --------------------------------------------
if not exist %VDrive%\ (
	echo ����㠫�� ��� %VDrive% �� ������!
	GOTO :Error
)

if "%1"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ��� 䠩��!
	GOTO :Error
)

if "%2"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ���� ��஢����!
	GOTO :Error
) else (
	set key=%2
)

if not exist %1 (
	echo ���� %1 �� ������!
	GOTO :Error
)

:: �஢�ઠ ��⥩ � ���砬 -----------------------------------
set key_path=%scr_path%Verba\%key%\Profile
if not exist %key_path% (
	echo ���� %key_path% �� ������!
	GOTO :Error
)
set skey_path=%scr_path%Verba\%key%\floppy
if not exist %skey_path% (
	echo ���� %skey_path% �� ������!
	GOTO :Error
)

:: ����㧪� ���� ��஢���� -------------------------------------
%wftesto% l > nul
if errorlevel 1 asrkeyw

call :GetKeySign

:: ������뢠�� ------------------------------------------------------
echo ������뢠�� ���箬 %key%01
%wftesto% s %1 %1 %key_path%\ %key%01

if not [%3]==[] (
	echo.
	echo  [Press any key to exit]
	pause > nul
)
EXIT /b 0

:: ���� �� ⠪�� ���� �।� ����㦥����? --------------------------------
:GetKeySign
FOR /F "tokens=1,2 delims=: " %%a IN ('%wftesto% l') DO (
	if "%%a"=="NUMP" (
		if "%%b"=="%key%01" (
			echo ���� 㦥 ����㦥�
			goto :eof
		)
	)
)
echo ����㦠�� ���� %key%01
RMDIR /s /q %VDrive%\
XCOPY %skey_path% %VDrive%\ /S /Y /Q > nul 2<&1
%wftesto% i %key%01 %VDrive%
if errorlevel 1 GOTO :Error
RMDIR /s /q %VDrive%\
goto :eof

:: ------------------------------------------------------
:Error
echo.
echo  [Press any key to exit]
pause > nul
