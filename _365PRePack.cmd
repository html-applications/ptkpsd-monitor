@ECHO OFF
:: 365PRePack
:: Version: 1.2.0
:: Author: mozers
:: ------------------------------------------------
:: Description:
:: ��९����뢠�� ��襤訩 䠩� 365 ��� (�᫨ �� ᮤ�ন� 䠩�� �襭�� - vrb)
:: �.�. �ᯠ���뢠��, ����஢뢠�� �������� 䠩�� � ᭮�� �������뢠�� "��� �뫮" (�� 㦥 ��� ��஢����)
:: �맮�: _365PRePack.cmd mz301022.745
:: �᫨ � ���뫪� ᮤ�ঠ��� vrb 䠩��, � ����頥� errorlevel 365
:: � ������ � HKCU\Environment\365 ��� ��᫠����� arj-䠩��
:: ------------------------------------------------
TITLE %~n0

:: �஢�ઠ �室��� ��ࠬ��஢ --------------------------------------------
if "%1"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ��� 䠩��!
	GOTO :Error
)

if not exist %1 (
	echo ���� %1 �� ������!
	GOTO :Error
)

SET CAB=%~dp0bin\cabarc.exe
SET ARJ=%~dp0bin\arj32.exe
SET vrb=0
:: --------------------------------------------
SET mtmp=%TEMP%\$mtmp$
IF EXIST %mtmp% (DEL /Q /F %mtmp%\*.*) ELSE (MD %mtmp%)
PUSHD %mtmp%

rem ��ᯠ����� �ᥣ� ᮤ�ন���� cab ��娢� (� ��� - �����⢥��� arj)
%CAB% -o X %1
IF NOT EXIST *.arj GOTO :no_vrb

FOR /R . %%A IN (*.arj) DO (
	rem ����砥� ��� arj-䠩��
	SET arj_fullname=%%A
	SET arj_name=%%~nxA
	rem ��ᯠ���뢠�� arj-䠩� (� ��� - vrb 䠩��)
	%ARJ% e %%A -y
	DEL /Q /F %%A
)
IF NOT EXIST *.vrb GOTO :no_vrb

rem ��१ ॥��� ��।��� ��� arj-䠩�� � �᭮���� �ਯ�
REG ADD "HKCU\Environment" /v 365 /d %arj_name% /f

rem �����஢�� ��� vrb 䠩���
FOR /R . %%V IN (*.vrb) DO (
	SET vrb=365
	cscript "%~dp0\_SignDelete.js" %%V
	IF errorlevel 1 GOTO :Error
	CALL "%~dp0\_Decrypt.cmd" %%V
	IF errorlevel 1 GOTO :Error
)

IF "%vrb%"=="365" (
	rem ������뢠�� �� vrb 䠩�� � arj
	%ARJ% a -e %arj_fullname% *.vrb
	IF errorlevel 1 GOTO :Error
	DEL /Q /F *.vrb
	rem ������뢠�� arj 䠩� � cab
	DEL /Q /F %1
	%CAB% N %1 *.arj
	IF errorlevel 1 GOTO :Error
)

:no_vrb
DEL /Q /F *.*
POPD

if not [%2]==[] (
	echo.
	echo  [Press any key to exit]
	pause > nul
)
EXIT %vrb%

:: ------------------------------------------------------
:Error
echo.
echo  [Press any key to exit]
pause > nul
EXIT 1
