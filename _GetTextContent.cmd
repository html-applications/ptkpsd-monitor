@ECHO OFF
:: GetTextContent
:: Version: 1.0.0
:: Author: mozers
:: ------------------------------------------------
:: Description:
:: �����頥� (�१ StdOut) ᮤ�ন��� 䠩��� (㯠������� "�� ����" �ᯠ���뢠����)
:: �맮�: _GetTextContent.cmd mz301022.745
:: ------------------------------------------------

:: �஢�ઠ �室��� ��ࠬ��஢ --------------------------------------------
if "%1"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ��� 䠩��!
	GOTO :Error
)

if not exist %1 (
	echo ���� %1 �� ������!
	GOTO :Error
)

SET unpacker=%~dp0\bin\7z.exe
SET mtmp=%TEMP%\$mtmp$
IF EXIST %mtmp% (DEL /Q /F %mtmp%\*.*) ELSE (MD %mtmp%)
PUSHD %mtmp%
CALL :Extract %1
DEL /Q /F *.*
POPD

if not [%2]==[] (
	echo.
	echo  [Press any key to exit]
	pause>nul
)
EXIT /B

:: ------------------------------------------------------
:Error
echo.
echo  [Press any key to exit]
pause>nul
EXIT 1

:Extract
%unpacker% l %1>nul
IF NOT ERRORLEVEL 1 (
	FOR /F "tokens=1,2 skip=5" %%A IN ('%unpacker% e -y %1') DO (
		IF "%%A"=="Extracting" CALL :Extract %%B
	)
) ELSE (
	ECHO ###%1###
	MORE < %1
)
