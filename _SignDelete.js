/*
Signature Delete
Version: 2.0.0
Author: mozers�
------------------------------------------------
Description:
������� ������� � �����
�������������: _SignDelete.js filename
------------------------------------------------
*/

var FSO = new ActiveXObject("Scripting.FileSystemObject");
var Stream = new ActiveXObject("ADODB.Stream");

// ������ �����
// ���� ���-�� ���� (byte_read) �� �������, �� �������� ���� ����
function ReadFile(filename, byte_read) {
	var content = '';
	if (FSO.FileExists(filename)) {
		if (FSO.GetFile(filename).Size > 0) {
			with (Stream) {
				type = 2; // adTypeText
				charset = 'windows-1251';
				open();
				loadFromFile(filename);
				content = (byte_read) ? readText(byte_read) : readText();
				close();
			}
		}
	}
	return content;
}

// ���������� ������ � �����
function SaveFile(filename, text) {
	with (Stream) {
		Type = 2; // adTypeText
		charset = 'windows-1251';
		Open();
		WriteText (text);
		SaveToFile (filename, 2);
		Close();
	}
}

var arg = WScript.Arguments;
if (arg.length == 0) {
	WScript.Echo('Require argument - filename!');
	WScript.Quit(1);
}
var filepath = arg(0);
if (!FSO.FileExists(filepath)) {
	WScript.Echo('File ' + filepath + ' not found!');
	WScript.Quit(1);
}
var filename = FSO.GetFileName(filepath);

var content = ReadFile(filepath);
if (/o000000(?:.|\n){64}(\d{4}[0-9A-Z]{6}\d{2})(?:.|\n){15}$/.test(content)) {
	SaveFile(filepath, RegExp.leftContext);
	WScript.Echo('Signature ' + RegExp.$1 + ' deleted in file ' + filename);
}else{
	WScript.Echo('No sign in file ' + filename);
}
