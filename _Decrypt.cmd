@echo off
:: Decrypt
:: Version: 1.1.0
:: Author: mozers
:: ------------------------------------------------
:: Description:
:: �����஢뢠�� 䠩� (����室�� ����㠫�� ��᪮���)
:: �ᯮ�짮�����: _Decrypt.cmd filename
:: ------------------------------------------------

set VDrive=B:
set scr_path=%~dp0
SET wftesto=%~dp0bin\wftesto.exe
TITLE %~n0

:: �஢�ઠ �室��� ��ࠬ��஢ --------------------------------------------
if not exist %VDrive%\ (
	echo ����㠫�� ��� %VDrive% �� ������!
	GOTO :Error
)

if "%1"=="" (
	echo �������� ��易⥫�� ��ࠬ��� - ��� 䠩��!
	GOTO :Error
)

if not exist %1 (
	echo ���� %1 �� ������!
	GOTO :Error
)

:: ��室�� ���� ��஢���� ---------------------------------------------------
FOR /F "tokens=1,2" %%a IN ('cscript %scr_path%_GetCrypto.js %1') DO (
	if "%%a"=="key" SET key=%%b
	if "%%a"=="to" SET to=%%b
	if "%%a"=="sign" SET sign=%%b
)

if "%key%"=="" (
	echo ���� %1 �� ����஢��!
	GOTO :Error
)

set key=%to%%key%
echo ���� %1 ����஢�� ���箬 %key%

if not "%sign%"=="" (
	echo ���� %1 �����ᠭ.
	echo ��। ����஢����� ᭨��� �������!
	GOTO :Error
)

:: �஢�ઠ ��⥩ � ���砬 -----------------------------------
set key_path=%scr_path%Verba\%key%\Profile
if not exist %key_path% (
	echo ���� %key_path% �� ������!
	GOTO :Error
)
set skey_path=%scr_path%Verba\%key%\floppy
if not exist %skey_path% (
	echo ���� %skey_path% �� ������!
	GOTO :Error
)

:: ����㧪� ���� ��஢���� -------------------------------------
%wftesto% l > nul
if errorlevel 1 asrkeyw

call :GetSlotKey

if not "%exist_key%"=="%key%" (
	if not "%exist_key%"=="" (
		echo ����塞 ���� ���� �� 0 ᫮�
		%wftesto% r %exist_key%
		if errorlevel 1 GOTO :Error
	)
	echo ����㦠�� � 0 ᫮� ���� %key%
	RMDIR /s /q %VDrive%\
	XCOPY %skey_path% %VDrive%\ /S /Y /Q > nul 2<&1
	%wftesto% i %key% %VDrive%
	if errorlevel 1 GOTO :Error
	RMDIR /s /q %VDrive%\
)

:: �����஢�� ---------------------------------------------------
echo �����஢뢠�� ���箬 %key%
%wftesto% d %1 %1 %key_path%\ %key_path% %to%

if not [%2]==[] (
	echo.
	echo  [Press any key to exit]
	pause > nul
)
EXIT /b 0

:: ����� ���� ����㦥� � 0 ᫮� ? --------------------------------
:GetSlotKey
FOR /F "tokens=1,2 delims=: " %%a IN ('%wftesto% l') DO (
	if "%%a"=="Slot" (if not "%%b"=="0" goto :eof)
	if "%%a"=="NUM" (
		echo � 0 ᫮� - ���� %%b
		SET exist_key=%%b
		goto :eof
	)
)
echo � 0 ᫮� - ��� ���祩
goto :eof

:: ------------------------------------------------------
:Error
echo.
echo  [Press any key to exit]
pause > nul
EXIT 1
